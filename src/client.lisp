;;;; client.lisp
(in-package #:dpo)

(defvar *conn* nil "The connection variable.")

(defmacro with-dpo-connection
    ((var &key host port (stream t)) &body body)
  `(let ((,var (lime:make-connection ,host ,port)))
     (format ,stream "Connecting DPO server ~A:~A." ,host ,port)
     (lime:connect ,var)
     (format ,stream "DPO server running on ~A:~A connected.~%" host port)
     (unwind-protect (progn ,@body)
       (swank:stop-server ,port))))

(defun dpo-repl (host port)
  "Start a trivial DPO repl."
  (with-dpo-connection (*conn* :host host :port port)
    (loop;; Read all events
      (sleep 0.05)
      (let ((events (lime:pull-all-events *conn*)))
        (loop for event in events do
          (typecase event
            (lime:write-string-event
             (write-string (lime:event-string event)))
            (lime:debugger-event
             (write-string "Entered debugger!"))
            (t
             t))))
       ;; Take input
       (if (lime:connection-reader-waiting-p *conn*)
           ;; Read a line to send as standard input
           (progn
             (format t "~%Read: ")
             (let ((input (read-line)))
               (lime:send-input *conn* input)))
           ;; Read regular code
           (progn
             (format t "~A> " (lime:connection-package *conn*))
             (let ((input (read)))
               (lime:evaluate *conn*
                              (with-standard-io-syntax
                                (prin1-to-string input)))))))))
