;;;; server.lisp
(in-package #:dpo)

(defvar *physical-ip-addr* nil
  "The physical ip address of current machine, will be set up during building.")

(defvar *port* nil
  "The port number swank server is using.")

;;; Network
(defun start-swank (ip port)
  (setf swank:*configure-emacs-indentation* nil)
  (let ((swank::*loopback-interface* ip))
    (swank:create-server :port port :dont-close t)))

(defun start-dpo-server (src)
  (let (src-path)
    (if (find #\/ src :test 'char=)
        (setq src-path (uiop:ensure-pathname src))
      (setq src-path
	    (merge-pathnames src (uiop:getcwd))))
    (let ((ip *physical-ip-addr*)
	  (port (find-port)))
      (make-dpo src-path)
      (setq *port* port)
      (format t "Starting DPO instance ~A ...~%"
              (dpo-id *dpo*))
      (start-swank ip port)
      (sleep 1)
      (format t "~%DPO instance is host by ~A and running on ~A:~A, PID: ~A"
	      (lisp-implementation-type)
	      ip port (swank::getpid)))))

(defun stop-dpo-server (&optional (stream *standard-output*))
  (format stream "~&Shutdown DPO instance ...~%")
  (swank:stop-server *port*)
  (format stream "DPO ~A has been killed.~%" (dpo-id *dpo*)))
