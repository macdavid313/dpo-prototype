;;;; dpo.lisp
(in-package #:dpo)

(defvar *dpo* nil "The DPO instance, will be used by server.")

(defvar +lock+ nil "The very mutex for a DPO instance.")

(defclass dpo ()
  ((id :reader dpo-id :type simple-string
       :documentation "Serial ID generated for a DPO.")
   (ipv6-addr :initform (generate-dpo-ipv6-addr) :reader dpo-ipv6-addr
              :type (simple-array character (39))
              :documentation "IPv6 Address randomly generated for a DPO.")
   (analysis-type :initarg :analysis-type :accessor dpo-analysis-type
                  :type simple-string
                  :documentation "Analysis Type of a DPO.")
   (tab :initarg :tab :accessor dpo-tab :type simple-string
        :documentation "The main Tab for a DPO.")
   (subtab :initarg :subtab :accessor dpo-subtab :type simple-string
           :documentation "The subtab for a DPO.")
   (authors :initarg :authors :accessor dpo-authors :type list
            :documentation "A list of author names of a DPO.")
   (publishing-date :initarg :publishing-date :accessor dpo-publishing-date :type function
                    :documentation "The publishing date of a DPO.")
   (title :initarg :title :accessor dpo-title :type simple-string
          :documentation "The title of a DPO.")
   (subtitle :initarg :subtitle :accessor dpo-subtitle :type simple-string
             :documentation "The subtitle of a DPO.")
   (target-audience :initarg :target-audience :accessor dpo-target-audience
                    :type list :documentation "The target audience of a DPO.")
   (core-statement :initarg :core-statement :accessor dpo-core-statement
                   :type simple-string :documentation "The core statement of a DPO.")
   (consequences :initarg :consequences :accessor %dpo-consequences :type list
                 :documentation "A list of consequences of a DPO.")
   (evidences :initarg :evidences :accessor %dpo-evidences :type vector
                 :documentation "A list of evidences of a DPO.")
   (analysis :initarg :analysis :accessor %dpo-analysis :type vector
             :documentation "A list of analysis of a DPO.")
   (references :initarg :references :accessor %dpo-references :type vector
               :documentation "A list of references of a DPO.")
   (original-data :initarg :original-data :accessor dpo-original-data :type hash-table
                  :documentation "The original data of a DPO, recorded in a hash table parsed from YAML."))
  (:documentation "The basic definition of a DPO."))

(defmethod initialize-instance :after ((obj dpo) &key)
  (with-slots (id title subtitle core-statement) obj
    (setf id (generate-dpo-id (concatenate 'string title subtitle core-statement)))))

(defclass dpo-sentence ()
  ((scope :initarg :scope :accessor dpo-sentence-scope :type keyword
          :documentation "The scope of a sentence, could be ':core-statement', ':consequence', ':evidence' or ':analysis'.")
   (text :initarg :text :accessor dpo-sentence-text :type simple-string
             :documentation "The original raw text of the sentence.")
   (linked-to :initarg :linked-to :accessor dpo-sentence-linked-to :type (or list nil)
              :documentation "How this sentences is linked to other sentence."))
  (:documentation "Sentences, the basic unit of paragraphs from a DPO."))

(defclass dpo-reference ()
  ((link :initarg :link :accessor dpo-reference-link :type simple-string
         :documentation "The original link of the referenece")
   (retrieved-date :initarg :retrived-date :accessor dpo-referece-retrieved-date
                   :type simple-string :documentation "The retrieved date of the reference."))
  (:documentation "The definition of references of a DPO."))

;;; APIs
(defun make-dpo (source)
  (declare (type hash-table source))
  (let* ((table (read-dpo-from-source source))
	 (dpo (apply 'make-instance 'dpo
		     (alexandria:hash-table-plist table))))
    (prog1 dpo
      (setq *dpo* dpo)
      (setq +lock+
	    (bt:make-lock (format nil "DPO-~A" (dpo-id *dpo*)))))))

;; (defmethod dpo-consequences ((dpo dpo) &optional (index nil index-p))
;;   (bt:with-lock-held (+lock+)

;;; Utilities
(eval-when (:compile-toplevel :load-toplevel :execute)
  (defun generate-dpo-id (str)
    (declare (optimize speed (safety 0) (space 0)))
    (let (high low)
      (declare (dynamic-extent high low))
      (multiple-value-setq (high low)
        (city-hash-128 (babel:string-to-octets str)))
      (let ((*print-base* 16)
            (str (make-array 32 :element-type 'base-char :adjustable nil :fill-pointer 0))
            (res (make-array 36 :element-type 'base-char :adjustable nil :fill-pointer 0)))
        (with-output-to-string (o str :element-type 'base-char)
          (princ (logior (ash high 64) low) o))
        (loop for i from 0 to 31
           do (progn
                (when (or (= i 8) (= i 12) (= i 16) (= i 20))
                  (vector-push #\- res))
                (vector-push (char str i) res))
           finally (return res)))))

  (defun generate-dpo-ipv6-addr ()
    "Randomly generate ipv6 addresses."
    (declare (optimize speed (safety 0) (space 0)))
    (flet ((bit-vector->integer (bit-vector)
             "Create a positive integer from a bit-vector."
             (reduce (lambda (x y)
                       (declare (type bit y)
                                (type (integer 0 #.(expt 16 4)) x))
                       (+ (* x 2) y))
                     bit-vector)))
      (let* ((vec-len #.(- 128 32))
             (vec (make-array vec-len :element-type 'bit :adjustable nil)))
        (dotimes (i vec-len)
          (setf (sbit vec i) (random 2)))
        (let ((rest-part (loop for offset fixnum = 0 then (+ offset 16)
                            while (< offset vec-len) collect
                              (let* ((16bits (make-array 16 :element-type 'bit :displaced-to vec :displaced-index-offset offset))
                                     (number (bit-vector->integer 16bits))
                                     (str (format nil "~4X" number)))
                                (substitute #\0 #\Space str :test 'char=)))))
          (concatenate 'string "2001:CAFE:"
                       (format nil "~{~A~^:~}" rest-part))))))
  ) ;;; end of eval-when

(defun lispyfy-dpo-keys (key)
  (declare (type simple-string key)
           (optimize speed (safety 0) (space 0)))
  (let ((key (string-upcase key)))
    (cond ((string= "AUTHOR(S)" key) :authors)
          ((find #\Space key :test 'char=)
           (intern (substitute #\- #\Space key :test 'char=) :keyword))
          (t (intern key :keyword)))))

(defvar *scanner/sentence*
  (cl-ppcre:create-scanner "(\\s*\\[[AE]?[0-9,\\s]+\\]\\s*)+"
   :case-insensitive-mode t)
  "Match (tokenize) all tags among one piece of text.")

(defvar *scanner/match-multi-tags*
  (cl-ppcre:create-scanner "\\[[AE]?[0-9,\\s]+\\]"
                           :case-insensitive-mode t)
  "Match multi tags for one sentence,
e.g. \"[A2] [E1, 2, 3]\" => (\"[A2]\" \"[E1, 2, 3]\")")

(defvar *scanner/split-by-comma*
  (cl-ppcre:create-scanner "\\s*,\\s*")
  "Split comma separated indices of a tag.")

(defun dpo-text-tokenizer (text)
  (labels ((trim-num (text)
             (let ((pos-of-1st-space (position #\Space text :test 'char=)))
               (subseq text (+ 1 pos-of-1st-space))))
           (process-tags (tags)
             (let ((tags-list (ppcre:all-matches-as-strings *scanner/match-multi-tags* tags)))
               (mapcar (lambda (tag)
                         (let* ((str (string-trim '(#\[ #\]) tag))
                                (scope* (char str 0))
                                (scope (cond ((char-equal scope* #\A) :analysis)
                                             ((char-equal scope* #\E) :evidence)
                                             (t :references)))
                                (indices
                                 (mapcar 'parse-integer
                                         (ppcre:split *scanner/split-by-comma*
                                                      (if (eq scope :references)
                                                          str (subseq str 1))))))
                           (cons scope indices)))
                       tags-list))))
    (let* ((text (trim-num text))
           (indices (ppcre:all-matches *scanner/sentence* text))
           res)
      (loop with start* = 0
         for lst = indices then (cddr lst)
         while lst do (let* ((start (first lst))
                             (end (second lst))
                             (sentence (subseq text start* start))
                             (tags (subseq text start end)))
                        (setq start* end)
                        (push (cons sentence (process-tags tags)) res))
           finally (return (nreverse res))))))

(defun make-dpo-sentences-from-content (scope content &optional (output 'vector))
  (map output (lambda (sts)
                (loop for st in sts
                   collect (make-instance 'dpo-sentence :scope scope
                                          :text (first st)
                                          :linked-to (rest st))))
       (mapcar 'dpo-text-tokenizer content)))

(defun parse-references (lst &optional (output 'vector))
  (map output (lambda (ref)
                (let* ((split (ppcre:split "\\s+retrieved on\\s+" ref))
                       (link* (first split))
                       (link (let ((pos-of-1st-space (position #\Space link* :test 'char=)))
                               (subseq link* (+ 1 pos-of-1st-space))))
                       (date (second split)))
                  (make-instance 'dpo-reference :link link
                                 :retrived-date date)))
       lst))

(defun read-dpo-from-source (source)
  (when (pathnamep source)
    (setq source (uiop:read-file-string source)))
  (let* ((table (yaml:parse source))
         (keys-str (alexandria:hash-table-keys table))
         (keys-sym (mapcar 'lispyfy-dpo-keys keys-str))
         (res (make-hash-table :test 'eq)))
    (loop for str in keys-str
       for sym in keys-sym
       do (case sym
            (:authors (let* ((val (gethash str table))
                             (authors (ppcre:split *scanner/split-by-comma* val)))
                        (setf (gethash sym res) authors)))
            ((:consequences :evidences :analysis)
             (let* ((content (gethash str table))
                    (sentences (make-dpo-sentences-from-content sym content)))
               (declare (type list content))
               (setf (gethash sym res) sentences)))
            (:references
             (let ((ref-lst (gethash str table)))
               (setf (gethash sym res) (parse-references ref-lst))))
            (t (setf (gethash sym res) (gethash str table))))
       finally (progn
                 (setf (gethash :original-data res) table)
                 (return res)))))
