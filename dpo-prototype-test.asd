#|
  This file is a part of dpo-prototype project.
  Copyright (c) 2017 SCGEE (david.gu@scgee.com)
|#

(in-package :cl-user)
(defpackage dpo-prototype-test-asd
  (:use :cl :asdf))
(in-package :dpo-prototype-test-asd)

(defsystem dpo-prototype-test
  :author "SCGEE"
  :license ""
  :depends-on (:dpo-prototype
               :prove)
  :components ((:module "t"
                :components
                ((:test-file "dpo-prototype"))))
  :description "Test system for dpo-prototype"

  :defsystem-depends-on (:prove-asdf)
  :perform (test-op :after (op c)
                    (funcall (intern #.(string :run-test-system) :prove-asdf) c)
                    (asdf:clear-system c)))
