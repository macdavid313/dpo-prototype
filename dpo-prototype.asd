#|
  This file is a part of dpo-prototype project.
  Copyright (c) 2017 SCGEE (david.gu@scgee.com)
|#

#|
  The prototype implementation of DPO and its "Universe" (Runtime).

  Author: SCGEE (david.gu@scgee.com)
|#

(in-package #:cl-user)
(defpackage #:dpo-prototype-asd
  (:use #:cl #:asdf))
(in-package #:dpo-prototype-asd)

(defsystem dpo-prototype
  :version "0.1"
  :author "SCGEE"
  :depends-on (#:alexandria
               #:split-sequence
               #:cl-ppcre
               #:cffi
               #:cl-yaml
               #:city-hash
	       #:bordeaux-threads
               #:lime
               #:find-port
               #:daemon)
  :components ((:module "src"
                :components
                ((:file "package")
		 (:file "dpo")
		 (:file "server")
		 (:file "client")
		 (:file "universe")
		 (:file "dpo-prototype"))))
  :description "The prototype implementation of DPO and its \"Universe\" (Runtime)."
  :long-description
  #.(with-open-file (stream (merge-pathnames
                             #p"README.markdown"
                             (or *load-pathname* *compile-file-pathname*))
                            :if-does-not-exist nil
                            :direction :input)
      (when stream
        (let ((seq (make-array (file-length stream)
                               :element-type 'character
                               :fill-pointer t)))
          (setf (fill-pointer seq) (read-sequence seq stream))
          seq)))
  :in-order-to ((test-op (test-op dpo-prototype-test))))
